using System;
using System.Collections.Generic;

namespace WebBlazorWA.Models
{
    public sealed record JobRecord
    {
        public int Id { get; set; }
        public string Company { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public string Location { get; set; }
        public string Position { get; set; }
        public string ShortDescription { get; set; }
        public string Responsibilities { get; set; }


        public JobRecord()
        {
        }

        public JobRecord(int id, string company, DateTime? start, DateTime? end, string location, string position,
            string shortDescription, string responsibilities)
        {
            Id = id;
            Company = company;
            Start = start;
            End = end;
            Location = location;
            Position = position;
            ShortDescription = shortDescription;
            Responsibilities = responsibilities;
        }
    }
}