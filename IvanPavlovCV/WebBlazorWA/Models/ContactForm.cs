using System.ComponentModel.DataAnnotations;

namespace WebBlazorWA.Models
{
    public class ContactForm
    {
        [Required]
        [StringLength(30, ErrorMessage = "Name length is restricted by 30 symbols.")]
        public string Name { get; set; }
    }
}