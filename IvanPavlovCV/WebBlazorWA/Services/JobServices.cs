using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using WebBlazorWA.Models;

namespace WebBlazorWA.Services
{
    public class JobServices : IJobServices
    {
        private readonly HttpClient _httpClient;

        public JobServices(HttpClient httpClient) => _httpClient = httpClient;
        
        public async Task<List<JobRecord>> GetJobs() =>
        await _httpClient.GetFromJsonAsync<List<JobRecord>>("data/WorkExperience.json");

    }
}