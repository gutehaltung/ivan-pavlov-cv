using System.Collections.Generic;
using System.Threading.Tasks;
using WebBlazorWA.Models;

namespace WebBlazorWA.Services
{
    public interface IJobServices
    {
        public Task<List<JobRecord>> GetJobs();
    }
}